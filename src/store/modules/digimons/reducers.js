import { ADD_DIGIMONS, SHOW_ALL } from "./actionTypes";

const digimonsReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_DIGIMONS:
      return [action.digimon];
    case SHOW_ALL:
      return action.allDigimons;
    default:
      return state;
  }
};
export default digimonsReducer;
