import axios from "axios";
import { addDigimon, showDigimons } from "./actions";

export const addDigimonsThunk = (digimon, setError) => (dispatch) => {
  axios
    .get(`https://digimon-api.vercel.app/api/digimon/name/${digimon}`)
    .then((response) => {
      dispatch(addDigimon(...response.data));
    })
    .catch((err) => {
      //console.log(err.response.data.ErrorMsg);
      setError(err.response.data.ErrorMsg);
    });
};

export const showAllDigimonsThunk = () => (dispatch) => {
  axios
    .get(`https://digimon-api.vercel.app/api/digimon`)
    .then((response) => {
      dispatch(showDigimons(response.data));
    })
    .catch((err) => {
      console.log(err);
    });
};
