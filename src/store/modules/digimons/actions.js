import { ADD_DIGIMONS, SHOW_ALL } from "./actionTypes";

export const addDigimon = (digimon) => ({
  type: ADD_DIGIMONS,
  digimon,
});

export const showDigimons = (digimons) => ({
  type: SHOW_ALL,
  allDigimons: digimons,
});
