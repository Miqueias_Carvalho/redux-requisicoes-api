import styled from "styled-components";

export const Container = styled.div`
  ul {
    margin-top: 20px;
    display: flex;
    flex-wrap: wrap;

    li {
      width: 180px;
      height: 240px;
      background-color: white;
      border-radius: 5px;
      margin: 5px 8px;
      /* display: inline-block; */

      img {
        width: 75%;
        margin-top: 10px;
      }
      h3 {
        font-size: 20px;
        color: #352404;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
      p {
        color: #8a7144;
        font-size: 14px;
        margin-top: 8px;
      }
    }
  }
`;
