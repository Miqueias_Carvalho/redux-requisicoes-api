import { Container } from "./styles";
import { useSelector } from "react-redux";

function Digimons() {
  const { digimons } = useSelector((state) => state);

  return (
    <Container>
      <ul>
        {digimons.map((digimon, index) => (
          <li key={index}>
            <img src={digimon.img} alt="" />
            <h3>{digimon.name}</h3>
            <p>{digimon.level}</p>
          </li>
        ))}
      </ul>
    </Container>
  );
}

export default Digimons;
