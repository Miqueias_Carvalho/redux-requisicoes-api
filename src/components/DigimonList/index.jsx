import { Container } from "./styles";
import { useDispatch } from "react-redux";
import { showAllDigimonsThunk } from "../../store/modules/digimons/thunks";

function Digimonlist() {
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(showAllDigimonsThunk());
  };

  return (
    <Container>
      <button onClick={handleClick}>Mostrar Todos</button>
    </Container>
  );
}
export default Digimonlist;
