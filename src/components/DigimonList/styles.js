import styled from "styled-components";

export const Container = styled.div`
  button {
    margin-left: 5px;
    height: 42px;
    border-radius: 8px;
    width: 150px;
    font-size: 16px;
    background-color: #352404;
    color: orange;
  }
`;
