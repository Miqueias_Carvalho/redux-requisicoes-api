import styled from "styled-components";

export const Container = styled.div`
  h2 {
    text-shadow: 1px 6px 4px #352404;
    color: orange;
  }
  div {
    height: 80px;

    p {
      color: red;
      font-size: 14px;
      margin-bottom: 0;
    }
    input {
      height: 40px;
      border-radius: 8px;
      border: none;
      width: 200px;
      font-size: 16px;
      padding-left: 10px;
    }
    button {
      margin-left: 5px;
      height: 42px;
      border-radius: 8px;
      width: 100px;
      font-size: 16px;
      background-color: #352404;
      color: orange;
    }
  }
`;
