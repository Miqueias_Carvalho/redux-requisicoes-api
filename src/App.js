import "./App.css";
import Search from "./components/Search";
import Digimons from "./components/Digimons";
import Digimonlist from "./components/DigimonList";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Digimonlist />
        <Search />
        <Digimons />
      </header>
    </div>
  );
}

export default App;
